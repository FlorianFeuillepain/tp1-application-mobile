package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class CountryActivity extends AppCompatActivity {

    private ImageView mImageView;
    private TextView mCountryName;
    private CountryList mCountryList;

    private EditText mCapitale;
    private EditText mLangue;
    private EditText mMonnaie;
    private EditText mPopulation;
    private EditText mSuperficie;

    private Button mSaveButton;

    private Country mCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        mCountryList = new CountryList();
        mImageView = (ImageView) findViewById(R.id.activity_country_image);
        mCountryName =(TextView) findViewById(R.id.activity_country_countryName);

        mCapitale = (EditText) findViewById(R.id.activity_country_Capitale_EditText);
        mLangue = (EditText) findViewById(R.id.activity_country_Langue_EditText);
        mMonnaie = (EditText) findViewById(R.id.activity_country_Monnaie_EditText);
        mPopulation = (EditText) findViewById(R.id.activity_country_Population_EditText);
        mSuperficie = (EditText) findViewById(R.id.activity_country_Superficie_EditText);

        mSaveButton = (Button) findViewById(R.id.activity_country_save_button);

        Intent intent = getIntent();
        if(intent != null)
        {
            String nameCoutry = intent.getStringExtra("countryName");
            mCountryName.setText(nameCoutry);
            System.out.println(nameCoutry);
            mCountry = CountryList.getCountry(nameCoutry);
            int resID = getResources().getIdentifier("drawable/" + mCountry.getmImgFile(), null, getPackageName());
            mImageView.setImageResource(resID);
            mCapitale.setText(mCountry.getmCapital());
            mLangue.setText(mCountry.getmLanguage());
            mMonnaie.setText(mCountry.getmCurrency());
            mPopulation.setText(Integer.toString(mCountry.getmPopulation()));
            mSuperficie.setText(Integer.toString(mCountry.getmArea()) + " km2");
        }

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCountry != null)
                saveAllEditText();
            }
        });
    }

    private void saveAllEditText()
    {
        mCountry.setmCapital(mCapitale.getText().toString());
        mCountry.setmLanguage(mLangue.getText().toString());
        mCountry.setmCurrency(mMonnaie.getText().toString());
        mCountry.setmPopulation(Integer.valueOf(mPopulation.getText().toString()));
        mCountry.setmArea(Integer.valueOf(mSuperficie.getText().toString().split(" ")[0]));
    }
}
